<?php

declare(strict_types=1);

namespace Tests\DataFaker\Collections;

use Illuminate\Support\Collection;
use Tests\DataFaker\Models\DepositPrivateWalletOperationFixture;

class DepositPrivateWalletOperationCollectionFixture
{
    public static function get(): Collection
    {
        return new Collection([DepositPrivateWalletOperationFixture::get()]);
    }
}
