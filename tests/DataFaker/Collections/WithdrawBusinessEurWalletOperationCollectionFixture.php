<?php

declare(strict_types=1);

namespace Tests\DataFaker\Collections;

use Illuminate\Support\Collection;
use Tests\DataFaker\Models\WithdrawBusinessWalletOperationFixture;

class WithdrawBusinessEurWalletOperationCollectionFixture
{
    public static function get(): Collection
    {
        return new Collection([WithdrawBusinessWalletOperationFixture::get()]);
    }
}
