<?php

declare(strict_types=1);

namespace Tests\DataFaker\Collections;

use Illuminate\Support\Collection;
use Tests\DataFaker\Models\WithdrawPrivateEurWalletOperationFixture;

class WithdrawPrivateUserMadeFourActionCollection
{
    public static function get(): Collection
    {
        return new Collection([
            WithdrawPrivateEurWalletOperationFixture::get(),
            WithdrawPrivateEurWalletOperationFixture::get(),
            WithdrawPrivateEurWalletOperationFixture::get(),
            WithdrawPrivateEurWalletOperationFixture::get(),
        ]);
    }
}
