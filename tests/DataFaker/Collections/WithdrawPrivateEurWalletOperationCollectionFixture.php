<?php

declare(strict_types=1);

namespace Tests\DataFaker\Collections;

use Illuminate\Support\Collection;
use Tests\DataFaker\Models\WithdrawPrivateEurWalletOperationFixture;

class WithdrawPrivateEurWalletOperationCollectionFixture
{
    public static function get(): Collection
    {
        return new Collection([WithdrawPrivateEurWalletOperationFixture::get()]);
    }
}
