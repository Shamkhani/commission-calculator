<?php

declare(strict_types=1);

namespace Tests\DataFaker\Collections;

use Illuminate\Support\Collection;
use Tests\DataFaker\Models\WithdrawPrivateEurHighWalletOperationFixture;

class WithdrawPrivateHighAmountActionCollection
{
    public static function get(): Collection
    {
        return new Collection([
            WithdrawPrivateEurHighWalletOperationFixture::get(),
        ]);
    }
}
