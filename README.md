## How Run and Test:
#### 1. Copy .env.example 
`$ cp .env.example .env`

#### 2. run commands:
`$ docker-compose up -d`

`$ docker exec -it commission-calc bash`

#### 3. Need to install application dependencies
`$ composer install`
#### 4. System should be run by follow command: 
`$ php artisan commission:calculate input.csv`     

#### 5. To initiate system's tests 
`$ vendor/bin/phpunit`      
